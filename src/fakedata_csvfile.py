import csv
import os
from itertools import count
from faker import Faker
fake_data = Faker()

def reMap(fieldnames, listfield):
    address_count = count(1)
    for i in listfield:
        return [i + '{}'.format(next(address_count)) if f.startswith(i) else f
            for f in fieldnames]

def getFileName(iFile):
    fname = "de_identify" + os.path.basename(iFile)
    return fname

def deIdentify(iFile, oFile):
    with open(iFile, mode = "r") as inputfile, open(oFile, mode = "w") as outputfile:
        csv_reader = csv.reader(inputfile)
        csv_writer = csv.writer(outputfile)
        checked = True
        identifier = []
        listfield = ['subscriber_address_line']
        for row in csv_reader:
            if (checked):
                csv_writer.writerow(row)
                fieldnames = reMap(row, listfield)    
                checked = False            
            else:
                dicttest = dict(zip(fieldnames, row))
                dicttest['subscriber_last_name'] = fake_data.last_name()
                dicttest['subscriber_first_name'] = fake_data.first_name()
                dicttest['subscriber_middle_name_or_initial'] = fake_data.random_element(elements=('M', 'L', 'P', 'Q',''))
                dicttest['subscriber_name_suffix'] = fake_data.suffix()
                p_i = fake_data.random_number(digits = 10, fix_len = True)
                while  p_i in identifier:
                    p_i = fake_data.random_number(digits = 10, fix_len = True)
                dicttest['subscriber_primary_identifier'] = p_i
                identifier.append(p_i)
                dicttest['subscriber_city_name'] = fake_data.city()
                dicttest['subscriber_state_code'] = fake_data.military_state()
                dicttest['subscriber_postal_zone_or_zip_code'] = fake_data.postalcode_plus4()
                dicttest['subscriber_birth_date'] = fake_data.date_of_birth(minimum_age=41)
                dicttest['subscriber_gender_code'] = fake_data.random_element(elements=('M', 'F'))
                dicttest['subscriber_address_line1'] = fake_data.address()
                dicttest['subscriber_address_line2'] = fake_data.secondary_address()
                csv_writer.writerow(dicttest.values())

if __name__  == '__main__':
    deIdentify("../input/2010BA_DEIDENT_INTHB0837I_286230_10160249.csv","../output/" + getFileName("../input/2010BA_DEIDENT_INTHB0837I_286230_10160249.csv"))
    deIdentify("../input/2010BA_DEIDENT_INTPSV837P_286193_10140229.csv","../output/" + getFileName("../input/2010BA_DEIDENT_INTPSV837P_286193_10140229.csv"))
