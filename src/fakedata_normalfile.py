from faker import Faker
fake_data = Faker()
import os.path

def fake_row(string):
	data = string.split("*",9)
	data[3] = fake_data.first_name()
	data[4] = fake_data.last_name()
	data[9] = str(fake_data.random_number(digits=10, fix_len = True))
	a = "*".join(data) +"~\r\n"
	return a

def getFileName(iFile):
    fname = "de_identify" + os.path.basename(iFile)
    return fname

def deIdentify(iFile,oFile):
	inputfile = open(iFile, "r")
	outputfile = open(oFile, "w")

	rows = inputfile.readlines()
	for row in rows:
		outputfile.write(fake_row(row))
	
	inputfile.close()
	outputfile.close()

if __name__ == '__main__':
	deIdentify("../input/subscriber_name.txt", "../output/" + getFileName("../input/subscriber_name.txt"))